class SessionsController < ApplicationController

  skip_before_action :authorized, only: [:new, :create, :logout]

  def new
  end

  def create
    @user = User.find_by(username: params[:username])
    if @user && @user.authenticate(params[:password])
       session[:user_id] = @user.id
       redirect_to '/title/index'
    else
       redirect_to '/login'
    end
  end

  def login
  end

  def logout
    @user = nil
    session[:user_id] = nil
    redirect_to '/title/index'
  end

  def user_dashboard

  end
end
