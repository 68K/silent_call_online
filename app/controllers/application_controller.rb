class ApplicationController < ActionController::Base

  # By default all actions must be authorised.
  # Skip this for allowed actions in each controller.
  before_action :authorized

  # Let any controller check the current user.
  helper_method :current_user
  helper_method :logged_in?
  def current_user    
    User.find_by(id: session[:user_id])  
  end
  def logged_in?   
    !current_user.nil?  
  end

  def authorized
    redirect_to '/title/index' unless logged_in?
 end
end
