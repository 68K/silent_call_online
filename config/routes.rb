Rails.application.routes.draw do
  resources :users, only: [:new, :create]
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  get 'logout', to: 'sessions#logout'
  get 'title/index'

  root 'title#index'
  
  get 'dashboard', to: 'sessions#user_dashboard'
end
